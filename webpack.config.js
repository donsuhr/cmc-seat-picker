const webpack = require('webpack');
const path = require('path');
var CleanWebpackPlugin = require('clean-webpack-plugin');

const production = process.env.NODE_ENV === 'production';

const entry = {
    'cmc-seat-picker': [production ? path.join(__dirname, 'app/scripts/index.js') : path.join(__dirname, 'app/scripts/index-stub.js')],
}

const plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery',
        jQuery: 'jquery',
        Promise: 'core-js/es6/promise',
        fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        },
    }),
    new CleanWebpackPlugin(['dist'], {
        root: __dirname,
        verbose: true,
        dry: false,
    })
];
const output = {
    path: __dirname + '/dist',
    filename: 'cmc-seat-picker.js',
    publicPath: '/',
};
const externals = [];

if (production) {
    plugins.push(
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({
            mangle: true,
            output: {
                comments: false,
            },
            compress: {
                warnings: true,
                drop_debugger: false,
            },
        })
    );
    output.library = 'cmcSeatPicker';
    output.libraryTarget = 'commonjs';
    externals.push('jquery', 'query-string');

} else {
    // entry['cmc-seat-picker'].unshift('webpack/hot/dev-server', 'webpack-hot-middleware/client');
    plugins.push(
        new webpack.HotModuleReplacementPlugin()
    );
}

module.exports = {
    debug: !production,
    devtool: production ? 'source-map' : '#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),
    entry,
    plugins,
    resolve: {
        root: [path.join(__dirname, 'app')],
    },
    output,
    externals,
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['monkey-hot', 'babel'],
                include: [path.join(__dirname, '/app/')],
            },
            { test: /\.css$/, loader: "style!css" },
            {
                test: /\.scss$/,
                loaders: ["style", "css", "sass"]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'url-loader',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            }
        ]
    }
};
