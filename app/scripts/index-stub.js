import $ from 'jquery';
import cmcSeatPicker from './index'
cmcSeatPicker.init({
    $levelEl: $('[name="level"]'),
    $boothEl: $('[name="booth"]'),
    $usedEl: $('#booth'),
});
