import $ from 'jquery';
import 'jquery-seat-charts/jquery.seat-charts';
import queryString from 'query-string';

export default {
    init: function (options) {
        const $levelEl = options.$levelEl;
        const $boothEl = options.$boothEl;
        const $usedEl = options.$usedEl;

        var sc = $('#seat-chart').seatCharts({
            map: [
                'b[10,10]c[c]__c[d]b[25,25]',
                'a[9,9]a[11,11]__a[24,24]a[26,26]',
                'a[8,8]a[12,12]__a[23,23]a[27,27]',
                'a[7,7]a[13,13]__a[22,22]a[28,28]',
                'a[6,6]a[14,14]__a[21,21]a[29,29]',
                'a[5,5]a[15,15]__a[20,20]a[30,30]',
                'a[4,4]a[16,16]__a[19,19]a[31,31]',
                'a[3,3]a[17,17]__a[18,18]a[32,32]',
                'b[2,2]c[a]__c[b]b[1,1]',
            ],
            naming: {
                left: false,
                top: false,
            },
            seats: {
                a: {
                    classes: 'front-seat'
                },
                b: {
                    classes: 'back-seat'
                },
            },
            click: function () {
                let retVal;
                if (this.status() == 'available') {
                    const canTwoSmallTable = canBigTable();
                    const isSmallTable = this.settings.character === 'a';

                    if (isSmallTable && canTwoSmallTable) {
                        if (sc.find('b.selected').length) {
                            sc.find('selected').status('available');
                        }
                        $.each(sc.find('selected').seats, (index, element) => {
                            const selectedID = parseInt(this.settings.id, 10);
                            const elID = parseInt(element.settings.id, 10);
                            if (Math.abs(elID - selectedID) > 1) {
                                element.status('available');
                            }
                        })

                    } else {
                        sc.find('selected').status('available');
                    }
                    retVal = 'selected';
                } else if (this.status() == 'selected') {
                    retVal = 'available';
                } else if (this.status() == 'unavailable') {
                    //seat has been already booked
                    retVal = 'unavailable';
                } else {
                    retVal = this.style();
                }
                setTimeout(updateSelected, 200);

                return retVal;
            }
        });

        // sc.get(['18', '19', '20', '21', '22', '23']).status('unavailable');
        sc.get(['a', 'b', 'c', 'd']).node().css({ display: 'none' });

        function toggleBigTables() {
            sc.find('b').status(canBigTable() ? 'available' : 'unavailable');
        }

        function updateSelected() {
            let selected = sc.find('selected').seats.map((x) => x.settings.id);
            $boothEl.val(selected.join(','));
        }

        function canBigTable() {
            const value = $levelEl.val();
            return value === 'Diamond' ||
                value === 'Diamond - Plan A' ||
                value === 'Diamond - Plan B' ||
                value === 'Platinum' ||
                value === 'Platinum - Plan A' ||
                value === 'Platinum - Plan B';
        }

        function disableUsed() {
            const used = $usedEl.val().split(',').map(x => x.toString());
            sc.get(used).status('unavailable');
        }

        $levelEl.on('change', function (event) {
            toggleBigTables();
            const selected = sc.find('selected');
            const first = selected.seats[0];
            selected.status('available');
            if (first) {
                first.status('selected');
            }
            disableUsed();
            updateSelected();
        });

        $usedEl.on('change', function (event) {
            disableUsed();
        });

        const parsed = queryString.parse(window.location.search);
        const level = parsed.level;
        if (level) {
            $levelEl.val(level);
        }
        toggleBigTables();
        // $boothEl.css({ display: 'none' });
    }
};

